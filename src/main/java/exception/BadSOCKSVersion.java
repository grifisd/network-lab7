package exception;

public class BadSOCKSVersion extends SOCKSException {

    public BadSOCKSVersion(int version) {
        super("SOCKS version is " + version + " which is not currently supported");
    }
}
