package exception;

public class BadSOCKSReserved extends SOCKSException {
    public BadSOCKSReserved(int pos, byte realValue, byte expectedValue) {
        super(
                "Byte with position " + pos +
                        " must be reserved and equals to " + expectedValue +
                        " instead of " + realValue);
    }
}
