package exception;

public class BadSOCKSAuthMethod extends SOCKSException {
    public BadSOCKSAuthMethod() {
        super("All auth methods are not supported");
    }
}
