package exception;

public class BadSOCKSCommand extends SOCKSException{
    public BadSOCKSCommand(byte socksMethod) {
        super("Method provided " + socksMethod + " is not currently supported.");
    }
}
