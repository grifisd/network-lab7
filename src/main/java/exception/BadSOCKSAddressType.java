package exception;

public class BadSOCKSAddressType extends SOCKSException {
    public BadSOCKSAddressType(byte type) {
        super("Got address type " + type + " which currently is not supported");
    }
}
