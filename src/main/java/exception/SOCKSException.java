package exception;

public class SOCKSException extends Exception {
    SOCKSException(String message) {
        super(message);
    }

    SOCKSException() {
        super();
    }

}
