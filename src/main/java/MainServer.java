import common.utils.SelectionKeys;
import connection.*;
import exception.NotYetCompletedException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import storage.KeyValueStorage;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.*;
import java.util.Optional;
import java.util.Set;

import static common.utils.SelectionKeys.dstOf;

public class MainServer {
    //region Private fields
    private final static Logger log = LogManager.getLogger(MainServer.class);
    private final ServerSocketChannel serverSocket;
    private final Selector selector;
    private final KeyValueStorage<Connection> clientStorage;
    //endregion

    //region Initialization
    private MainServer(InetSocketAddress listenAddress) throws Throwable {
        selector = Selector.open();

        serverSocket = ServerSocketChannel.open();
        serverSocket.configureBlocking(false);
        serverSocket.register(selector, SelectionKey.OP_ACCEPT);
        serverSocket.bind(listenAddress);

        clientStorage = new KeyValueStorage<>();
    }
    //endregion

    //region Startup
    public static void main(String[] args)  {
        //region Args check
        if (args.length != 1) {
            printUsage();
            System.exit(0);
        }
        InetSocketAddress local;
        try {
            local = new InetSocketAddress("localhost", Integer.parseInt(args[0]));
        } catch (Exception e) {
            log.fatal(e);
            printUsage();
            System.exit(0);
            return;
        }
        //endregion

        //region Cycle start
        while (true) {
            try {
                new MainServer(local).start();
            } catch (Throwable thr) {
                log.error(thr);
                System.err.println("SOME FATAL ERROR HAPPENED. RESTARTING THE SERVER IN 5 SECONDS.");
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException ignored) {}
            }
        }
        //endregion
    }
    //endregion

    private void start() throws Throwable {
        log.debug(
                "\n\n===========SERVER STARTED! USING LOCALADDRESS "
                        + serverSocket.getLocalAddress()
                        + "==========\n\n");

        while (true) {
            selector.select();
            Set<SelectionKey> selectedKeys = selector.selectedKeys();
            log.trace("Selected " + selectedKeys.size() + " keys");

            for (SelectionKey key : selectedKeys) {
                if (!key.isValid()) {
                    log.warn("Bad key found! " + key);
                    continue;
                }

                //new attempt to connect from client
                if (key.isAcceptable()) {
                    accept(key);
                }

                //we can do finishConnect to somebody
                if (key.isConnectable()) {
                    connect(key);
                }

                if (key.isReadable()) {
                    try {
                        read(key);
                    } catch (NotYetCompletedException e) { continue; }
                }

                if (key.isWritable()) {
                    try {
                        write(key);
                    } catch (NotYetCompletedException ignored) { //noinspection UnnecessaryContinue
                        continue; }
                }
            }
            selectedKeys.clear();
        }
    }

    private void accept(SelectionKey key) throws IOException {
        log.debug("ACCEPT");
        SocketChannel clientSocket = serverSocket.accept();
        if (clientSocket == null) return;

        clientSocket.configureBlocking(false);

        //reading from client is available now
        SelectionKey srcKey = clientSocket.register(selector, SelectionKey.OP_READ);
        DNSConnection resolver = new DNSConnection(selector);
        //attach resolver key to the client
        srcKey.attach(resolver.getKey());
        //attach client key to the server
        resolver.getKey().attach(srcKey);

        SOCKS5Connection conn = new SOCKS5Connection(srcKey, selector, resolver);
        resolver.setOwner(conn);

        clientStorage.add(resolver.getKey(), resolver);
        clientStorage.add(srcKey, conn);
        log.info(
                "New attempt: " + clientSocket.getRemoteAddress()
                + ", total now: " + clientStorage.count());
    }

    private void connect(SelectionKey key) throws IOException {
        log.trace("CONNECT");
        boolean isConnected = ((SocketChannel) key.channel()).finishConnect();
        log.trace("connection.CompleteState: " + (isConnected ? "now is connected" : "not connected yet"));
        log.trace("key: " + key.channel() + " dstOf: " + dstOf(key).channel());
        SelectionKeys.opsOr(dstOf(key), SelectionKey.OP_READ);
        SelectionKeys.opsAnd(key, ~SelectionKey.OP_CONNECT);
    }

    private void read(SelectionKey key) throws NotYetCompletedException {
        log.trace("READ");
        Optional<Connection> connection = clientStorage.getAnyByKey(key);

        if (connection.isPresent()) {
            connection.get().readByKey(key);
        } else {
            //we have no such connection
            log.warn(
                    "No connection exists, but there's the key "
                            + key.toString()
                            + " with channel "
                            + key.channel()
                            + " who's interested in reading");
        }
    }

    private void write(SelectionKey key) throws NotYetCompletedException {
        log.trace("WRITE");
        Optional<Connection> connection = clientStorage.getAnyByKey(key);

        if (connection.isPresent()) {
            connection.get().writeByKey(key);
        } else {
            //we have no such connection
            log.warn(
                    "No connection exists, but there's the key "
                            + key.toString()
                            + " with channel "
                            + key.channel()
                            + " who's interested in writing");
        }
    }

    private static void printUsage() {
        System.out.println("Usage: java MainServer lport");
        System.out.println("Where:");
        System.out.println("\tlport - port on this machine " +
                "which will be used. Integer between 0-65535");
    }
}
