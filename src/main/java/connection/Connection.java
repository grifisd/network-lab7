package connection;

import common.SharedConstants;
import common.utils.SelectionKeys;
import exception.NotYetCompletedException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import storage.Removable;
import storage.RemoverDelegate;

import java.io.EOFException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;

import static common.utils.Logging.logBuffer;
import static common.utils.SelectionKeys.dstOf;
import static common.utils.SelectionKeys.opsAnd;

public abstract class Connection implements Removable {
    Logger log = LogManager.getLogger(this.toString());

    ByteBuffer inputBuffer = ByteBuffer.allocateDirect(SharedConstants.bufferSize);
    ByteBuffer outputBuffer = ByteBuffer.allocateDirect(SharedConstants.bufferSize);
    private Executor executor = new Executor();

    RemoverDelegate removerDelegate;

    //region Abstract methods
    abstract boolean isReadable();
    abstract boolean isWritable();

    abstract void enableReading(SelectionKey key);
    abstract void enableWriting(SelectionKey key);

    abstract void processReadData() throws IOException, NotYetCompletedException;
    abstract void write(SelectionKey key) throws IOException, NotYetCompletedException;
    //endregion

    final void disableReading(SelectionKey key) {
        SelectionKeys.opsAnd(key, ~SelectionKey.OP_READ);
    }

    final void disableWriting(SelectionKey key) {
        SelectionKeys.opsAnd(key, ~SelectionKey.OP_WRITE);
    }

    public final void readByKey(SelectionKey key) throws NotYetCompletedException {
        try {
            readInputBuffer(key.channel());
            processReadData();
        } catch (EOFException e) {
            log.warn("Got EOF. Stop reading IO operations");
            opsAnd(key, ~SelectionKey.OP_READ);
            throw new NotYetCompletedException();
        } catch (Exception e) {
            if (e instanceof NotYetCompletedException) {
                throw (NotYetCompletedException) e;
            }
            log.error(e.toString());
            log.error("Some error happened while reading the data. Disconnecting...");
            totalShutDown(key);
            throw new NotYetCompletedException();
        }
        if (!isReadable()) {
            disableReading(key);
        }
        if (isWritable()) {
            enableWriting(key);
        }
    }

    public final void writeByKey(SelectionKey key) throws NotYetCompletedException {
        try {
            write(key);
        } catch (Exception e) {
            if (e instanceof NotYetCompletedException) {
                throw (NotYetCompletedException) e;
            }
            log.error(e.toString());
            log.error("Some error happened while writing the data. Disconnecting...");
            totalShutDown(key);
            throw new NotYetCompletedException();
        }

        if (!isWritable()) {
            disableWriting(key);
        }
        if (isReadable()) {
            enableReading(key);
        }
    }

    private void totalShutDown(SelectionKey key) {
        disconnectByKey(key);
        removerDelegate.removeMe();
    }
    private void disconnectByKey(SelectionKey key) {
        log.trace("disconnectByKey " + key);
        try {
            if (key != null) key.cancel();
            else {
                log.error("Tried to disconnect on nullable key. Do nothing");
                return;
            }
            if (key.attachment() != null) dstOf(key).cancel();
            else log.info("This key.attachment() is null");

            if (key.channel() == null) {
                log.warn("This key.channel() is null");
                return;
            }
            key.channel().close();
        } catch (IOException e) {
            log.error("Some error happened while disconnecting the client: " + e);
        }
    }

    private int readInputBuffer(SelectableChannel from) throws IOException {
        log.debug("Connection.readInputBuffer()");
        int readCount;
        if (from instanceof SocketChannel) {
            log.trace("Reading from SocketChannel");
            SocketChannel socChan = (SocketChannel) from;
            log.trace(socChan.getLocalAddress() + " and remote: " + socChan.getRemoteAddress());
            readCount = socChan.read(inputBuffer);
        } else if (from instanceof DatagramChannel) {
            log.trace("Reading from DatagramChannel");
            DatagramChannel datChan = (DatagramChannel) from;
            log.trace(datChan.getLocalAddress() + " and remote: " + datChan.getRemoteAddress());
            readCount = datChan.read(inputBuffer);
        } else {
            log.error("Given unsupported SelectableChannel: " + from);
            throw new IOException("Bad SelectableChannel");
        }

        log.debug("Read " + readCount + " bytes:");
        logBuffer(inputBuffer);
        if (readCount == -1) {
            throw new EOFException();
        }
        return readCount;
    }

    int writeOutputBuffer(SelectableChannel to) throws IOException, NotYetCompletedException {
        executor.executeOnce(() -> outputBuffer.flip());
        int writeCount;
        if (to instanceof SocketChannel) {
            log.trace("Writing to SocketChannel");
            SocketChannel socChan = (SocketChannel) to;
            log.trace(socChan.getLocalAddress() + " and remote: " + socChan.getRemoteAddress());
            writeCount = socChan.write(outputBuffer);
        } else if (to instanceof DatagramChannel) {
            log.trace("Writing to DatagramChannel");
            DatagramChannel datChan = (DatagramChannel) to;
            log.trace(datChan.getLocalAddress() + " and remote: " + datChan.getRemoteAddress());
            writeCount = datChan.write(outputBuffer);
        } else {
            log.error("Given unsupported SelectableChannel: " + to);
            throw new IOException("Bad SelectableChannel");
        }

        log.debug("Write " + writeCount + " bytes:");
        logBuffer(inputBuffer);

        if (outputBuffer.remaining() > 0)
            throw new NotYetCompletedException();
        executor.reset();
        outputBuffer.clear();
        return writeCount;
    }

    @Override
    public final void setRemoverDelegate(RemoverDelegate removerDelegate) {
        this.removerDelegate = removerDelegate;
    }
}

class Executor {
    private boolean isDone = false;

    void executeOnce(Closure closure) throws IOException, NotYetCompletedException {
        if (!isDone)
            closure.run();
        //it throws NotYetCompletedException if not completed
        //and puts the flag to true, if exception didn't thrown -> action is completed
        isDone = true;
    }

    void reset() {
        isDone = false;
    }

    interface Closure {
        void run() throws IOException, NotYetCompletedException;
    }
}