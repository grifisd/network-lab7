package connection;

import exception.NotYetCompletedException;
import org.xbill.DNS.*;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.InetSocketAddress;
import java.nio.channels.*;
import java.util.Arrays;

import static common.utils.SelectionKeys.opsOr;

public class DNSConnection extends Connection {
    //region Private fields
    private final DatagramChannel datagramChannel;
    private final SelectionKey selectionKey;
    private boolean isReadable = false;
    private WeakReference<SOCKS5Connection> owner;
    private final Executor executor;
    //endregion

    //region Initialization
    public DNSConnection(Selector selector) throws IOException {
        try {
            datagramChannel = DatagramChannel.open();
        } catch (IOException e) {
            log.fatal("Could not start connection.DNSConnection: " + e);
            throw e;
        }
        String[] servers = ResolverConfig.getCurrentConfig().servers();
        log.debug("Found recursive DNS servers: " + Arrays.toString(servers));
        datagramChannel.configureBlocking(false);
        datagramChannel.connect(new InetSocketAddress(servers[0], 53));
        selectionKey = datagramChannel.register(selector, SelectionKey.OP_READ);

        executor = new Executor();
    }
    //endregion

    public SelectionKey getKey() {
        return selectionKey;
    }

    void startResolve(String domainName) throws IOException, NotYetCompletedException {
        executor.executeOnce( () -> {
                    Name name = Name.fromString(domainName);
                    Record question = Record.newRecord(name, Type.A, DClass.IN);
                    Message query = Message.newQuery(question);
                    byte[] requestBytes = query.toWire();
                    outputBuffer.put(requestBytes);
                });
        writeOutputBuffer(datagramChannel);
    }

    @Override
    void processReadData() throws IOException, NotYetCompletedException {
        log.trace("DNS_READ");
        byte[] bytes = new byte[inputBuffer.position()];
        inputBuffer.flip();
        inputBuffer.get(bytes);
        Message message = new Message(bytes);
        Record[] records = message.getSectionArray(Section.ANSWER);

        for (Record r : records) {
            if (r.getType() == Type.A) {
                byte[] ipAddr = ((ARecord)r).getAddress().getAddress();
                SOCKS5Connection unwrapped = owner.get();
                if (null == unwrapped) {
                    throw new IOException("Owner died");
                }
                unwrapped.isResolved(ipAddr);
                throw new NotYetCompletedException();
            }
        }
    }

    @Override
    void write(SelectionKey key) throws IOException, NotYetCompletedException {
        log.trace("DNS_WRITE");
        writeOutputBuffer(key.channel());
        //when writing completed, we can read
        isReadable = true;
    }

    public void setOwner(SOCKS5Connection owner) {
        this.owner = new WeakReference<>(owner);
    }

    @Override
    boolean isReadable() {
        return isReadable;
    }

    @Override
    boolean isWritable() {
        return outputBuffer.remaining() > 0;
    }

    @Override
    void enableReading(SelectionKey key) {
        opsOr(key, SelectionKey.OP_READ);
    }

    @Override
    void enableWriting(SelectionKey key) {
        opsOr(key, SelectionKey.OP_WRITE);
    }
}
