package connection;

import exception.*;
import common.SharedConstants;

import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Arrays;

import static common.utils.Logging.logBuffer;
import static common.utils.SelectionKeys.*;

public class SOCKS5Connection extends Connection {
    //region Common private fields
    private SOCKSCompletedState completedState = SOCKSCompletedState.start;
    private final SelectionKey clientKey;
    private SelectionKey dstKey;
    private final SocketChannel clientChannel;
    private SocketChannel dstChannel;
    private final DNSConnection resolver;
    private final Selector selector;
    private Executor executor = new Executor();

    private byte iterator = 0;
    private byte[] socksParams = new byte[SharedConstants.maxSOCKSParamLength];
    //endregion

    //region Socks dialogue params
    private Byte socksVersion = null;
    //TODO: move domainNameBytes and others to one array
    private Byte chosenAuthMethod = null;
    private Byte addressType = null;
    private byte[] domainNameBytes = null;
    private String fullQualifiedDomainName = null;
    private int dstPort;
    private byte[] ipAddressBytes = null;
    //endregion

    //region Initialization
    public SOCKS5Connection(SelectionKey key, Selector selector, DNSConnection resolver) {
        this.clientKey = key;
        this.resolver = resolver;
        this.selector = selector;
        clientChannel = (SocketChannel) key.channel();
    }
    //endregion

    //region SOCKS protocol dialogue
    private void readClientGreetingRequest() throws NotYetCompletedException, BadSOCKSVersion {
        log.trace("readClientGreetingRequest");
        if (null == socksVersion) {
            readBytes(socksParams, 2, 0, 0);
            verifySocksVersion(socksParams[0]);
            log.trace("authCount is " + socksParams[1]);
            inputBuffer.limit(socksParams[1] + 2); //set limit as 2 read bytes (socksVersion and authCount) + authCount
        }
        readBytes(socksParams, socksParams[1], 2, 2);
        verifySocksAuthMethod(socksParams, 2, socksParams[1]);

        clearBuffers();
        completedState = SOCKSCompletedState.clientGreetingRequest;
    }

    private void writeServerGreetingResponse() throws IOException, NotYetCompletedException  {
        log.trace("writeServerGreetingResponse");
        executor.executeOnce( () -> {
            outputBuffer.put(SharedConstants.supportedSOCKSVersion);
            outputBuffer.put(chosenAuthMethod);
        });
        writeOutputBuffer(clientChannel);
        executor.reset();
        if (chosenAuthMethod == SharedConstants.badSOCKSAuthMethodStub)
            throw new IOException("No chosen Auth method");
        //if it's OK, then change completedState
        clearBuffers();
        completedState = SOCKSCompletedState.serverGreetingResponse;
    }

    private void readClientConnectionRequest() throws IOException, NotYetCompletedException, BadSOCKSVersion, BadSOCKSCommand, BadSOCKSReserved, BadSOCKSAddressType {
        log.trace("readClientConnectionRequest");
        if (null == addressType) {
            readBytes(socksParams, 4, 0, 0);
            verifySocksVersion(socksParams[0]);
            verifySocksCommand(socksParams[1]); //TODO: not just refuse connection, but send SOCKS commandNotSupported error code
            verifyReserved(2, socksParams[2], (byte) 0x00);
            addressType = socksParams[3];
        }

        switch (addressType) {
            case SharedConstants.AddressType.IPv4:
                readIPv4Address();
                readPort(ipAddressBytes.length + 4);
            break;
            case SharedConstants.AddressType.DNS:
                readDomainName();
                executor.executeOnce(() -> resolver.startResolve(fullQualifiedDomainName));
                readPort(domainNameBytes.length + 5);
                executor.reset();
            break;
            case SharedConstants.AddressType.IPv6:
                throw new BadSOCKSAddressType(addressType); //TODO: support ipv6
                //break;
            default:
                throw new BadSOCKSAddressType(addressType);  //TODO: not just refuse connection, but send SOCKS addressNotSupported error code
        }

        clearBuffers();
        completedState = SOCKSCompletedState.clientConnectionRequest;
    }

    private void writeServerConnectionResponse() throws IOException, NotYetCompletedException {
        log.trace("writeServerConnectionResponse");

        if (null == ipAddressBytes) {
            throw new NotYetCompletedException();
        }

        executor.executeOnce(() -> {
                    outputBuffer.put(SharedConstants.supportedSOCKSVersion);
                    outputBuffer.put(SharedConstants.SOCKSStatus.requestGranted);
                    outputBuffer.put((byte) 0); //reserved

                    outputBuffer.put(SharedConstants.AddressType.IPv4);
                    outputBuffer.put(SharedConstants.zeroIPv4);

                    outputBuffer.put(SharedConstants.zeroPort);
                });

        writeOutputBuffer(clientChannel);
        executor.reset();
        outputBuffer.clear();

        setupForwarding();

        clearBuffers();
        completedState = SOCKSCompletedState.serverConnectionResponse;
    }
    //endregion

    //region Reading dst addresses
    private void readIPv4Address() throws NotYetCompletedException {
        if (null == ipAddressBytes) {
            inputBuffer.limit(4 + 4 + 2); //offset + ip bytes + port bytes
            readBytes(socksParams, 4, 4, 4);
            ipAddressBytes = new byte[4];
            System.arraycopy(socksParams, 4, ipAddressBytes, 0, 4);
        }
    }

    private void readDomainName() throws NotYetCompletedException {
        if (null == domainNameBytes) {
            readBytes(socksParams, 1, 4, 4); //reading length
            domainNameBytes = new byte[socksParams[4]];
            inputBuffer.limit(5 + domainNameBytes.length + 2); //offset + domain bytes + port bytes
        }
        readBytes(domainNameBytes, domainNameBytes.length, 0, 5);

        fullQualifiedDomainName = new String(domainNameBytes) +
                ( domainNameBytes[domainNameBytes.length - 1] == '.' ? "" : ".");
        log.debug("Got fullQualifiedDomainName: " + fullQualifiedDomainName);
    }

    private void readPort(int offset) throws NotYetCompletedException {
        readBytes(socksParams, 2, offset, offset);

        ByteBuffer portByteBuffer = ByteBuffer.allocateDirect(2);
        portByteBuffer.put(socksParams[offset]);
        portByteBuffer.put(socksParams[offset + 1]);
        portByteBuffer.flip();

        dstPort = portByteBuffer.getChar();
        log.debug("Got dstPort: " + dstPort);
    }

    private void readBytes(byte[] dst, int count, int dstOffset, int inputBufferOffset) throws NotYetCompletedException {
        while (iterator < count) {
            try {
                byte data = this.inputBuffer.get(iterator + inputBufferOffset);
                //log.trace("Read byte: " + String.format("0x%02X", data));
                dst[dstOffset + iterator++] = data;
            } catch (IndexOutOfBoundsException e) {
                log.debug("Read " + this.inputBuffer.position() + "/" + this.inputBuffer.limit() + " bytes");
                throw new NotYetCompletedException();
            }
        }
        log.trace("Byte array with " + count + " bytes successfully processed: " + Arrays.toString(dst));
        iterator = 0;
    }
    //endregion

    //region Forwarding
    private void setupForwarding() throws IOException {
        log.trace("setupForwarding");

        //remove DNS now!
        SelectionKey resolverKey = resolver.getKey();
        log.debug("Cancelling the resolver key: " + resolverKey.channel());
        resolverKey.channel().close();
        resolverKey.cancel();
        resolver.removerDelegate.removeMe();

        InetSocketAddress remoteAddress = new InetSocketAddress(InetAddress.getByAddress(ipAddressBytes), dstPort);
        dstChannel = SocketChannel.open();
        dstChannel.configureBlocking(false);
        dstKey = dstChannel.register(selector, SelectionKey.OP_CONNECT);
        dstChannel.connect(remoteAddress);

        //clientKey.interestOps(0);

        dstKey.attach(clientKey);
        clientKey.attach(dstKey);

        log.trace("srcKey: " + ((SocketChannel)clientKey.channel()).toString() + clientKey.interestOps());
        log.trace("dstKey: " + ((SocketChannel)dstKey.channel()).toString() + dstKey.interestOps());
        inputBuffer.clear();
    }

    private void writeForwarding(SelectionKey key) throws IOException {
        log.trace("writeForwarding");
        log.trace(dstChannel);
        log.trace("Buffer before flip():");
        logBuffer(inputBuffer);
        inputBuffer.flip();
        ((SocketChannel) key.channel()).write(inputBuffer);
        inputBuffer.compact();
    }
    //endregion

    //region Verification SOCKS packet data
    private void verifySocksVersion(byte version) throws BadSOCKSVersion {
        socksVersion = version;
        log.trace("Found SOCKS version: " + socksVersion);
        if (socksVersion != SharedConstants.supportedSOCKSVersion) {
            throw new BadSOCKSVersion(socksVersion);
        }
    }

    private void verifySocksAuthMethod(byte[] methods, int offset, int count) {
        for (int i = offset; i < count + offset; i++) {
            if (methods[i] == SharedConstants.supportedSOCKSAuthMethods[0]) {
                chosenAuthMethod = methods[i];
                log.info("Found supported method: " + String.format("0x%02X", methods[i]));
                return;
            }
            log.warn("Found not supported method: " + String.format("0x%02X", methods[i]));
        }
        log.error("Found no supported methods. Will send " + SharedConstants.badSOCKSAuthMethodStub + " next");
        chosenAuthMethod = SharedConstants.badSOCKSAuthMethodStub;
    }

    private void verifySocksCommand(byte cmd) throws BadSOCKSCommand {
        if (cmd != SharedConstants.supportedSOCKSMethods[0]) {
            throw new BadSOCKSCommand(cmd);
        }
    }

    private void verifyReserved(int pos, byte realValue, byte expectedValue) throws BadSOCKSReserved {
        if (realValue != expectedValue) throw new BadSOCKSReserved(pos, realValue, expectedValue);
    }
    //endregion

    //region Overridden from Connection
    @Override
    void processReadData() throws IOException {
        log.debug("SOCKS_READ");
        try {
            switch (completedState) {
                case start:
                    readClientGreetingRequest();
                    break;
                case serverGreetingResponse:
                    readClientConnectionRequest();
                    break;
                case serverConnectionResponse:
                    log.trace("readForwarding");
                    break;
                default:
                    log.error("Something has gone really wrong here.");
                    throw new IllegalStateException();
            }
        } catch (NotYetCompletedException e) {
            log.info("Have no data to complete READ operation. Waiting for more.");
        } catch (SOCKSException e) {
            log.error("SOCKS protocol mismatch: " + e.getMessage());
            throw new IOException(e.getMessage());
        }
    }

    @Override
    void write(SelectionKey key) throws IOException {
        log.debug("SOCKS_WRITE");
        try {
            switch (completedState) {
                case clientGreetingRequest:
                    writeServerGreetingResponse();
                    break;
                case clientConnectionRequest:
                    writeServerConnectionResponse();
                    break;
                case serverConnectionResponse:
                    writeForwarding(key);
                    break;
                default:
                    log.error("Something has gone really wrong here.");
                    throw new IllegalStateException();
            }
        } catch (NotYetCompletedException e) {
            log.debug("Couldn't complete WRITE operation. Trying one more time");
            log.debug("Remains " + outputBuffer.remaining() + " bytes");
            logBuffer(outputBuffer);
        }
    }

    @Override
    boolean isReadable() {
        switch (completedState) {
            case start:
                return true;
            case clientGreetingRequest:
                return false;
            case serverGreetingResponse:
                return true;
            case clientConnectionRequest:
                return false;
            case serverConnectionResponse:
                return inputBuffer.position() < inputBuffer.limit();
        }
        return false;
    }

    @Override
    boolean isWritable() {
        switch (completedState) {
            case start:
                return false;
            case clientGreetingRequest:
                return true;
            case serverGreetingResponse:
                return false;
            case clientConnectionRequest:
                return true;
            case serverConnectionResponse:
                return inputBuffer.position() > 0;
        }
        return false;
    }

    @Override
    void enableReading(SelectionKey key) {
        if (completedState == SOCKSCompletedState.serverConnectionResponse)
            opsOr(dstOf(key), SelectionKey.OP_READ);
        else
            opsOr(key, SelectionKey.OP_READ);
    }

    @Override
    void enableWriting(SelectionKey key) {
        if (completedState == SOCKSCompletedState.serverConnectionResponse)
            opsOr(dstOf(key), SelectionKey.OP_WRITE);
        else
            opsOr(key, SelectionKey.OP_WRITE);
    }
    //endregion

    private void clearBuffers() {
        Arrays.fill(socksParams, (byte)0);
        inputBuffer.clear();
        outputBuffer.clear();
    }

    void isResolved(byte[] ipAddress) throws IOException {
        ipAddressBytes = ipAddress;
        log.debug("Got address bytes: " + Arrays.toString(ipAddress));
        try {
            log.debug("Resolved address: " + InetAddress.getByAddress(ipAddress));
        } catch (UnknownHostException e) {
            log.error("Bad host resolved: " + e.getMessage());
            throw e;
        }
        try {
            writeServerConnectionResponse();
        } catch (NotYetCompletedException ignored) {}
    }
}

enum SOCKSCompletedState {
    start,

    clientGreetingRequest,
    serverGreetingResponse,

    clientConnectionRequest,
    serverConnectionResponse
}