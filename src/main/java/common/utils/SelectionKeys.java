package common.utils;

import java.nio.channels.SelectionKey;

public class SelectionKeys {
    public static SelectionKey opsAnd(SelectionKey key, int op) {
        return key.interestOps(key.interestOps() & op);
    }

    public static SelectionKey opsOr(SelectionKey key, int op) {
        return key.interestOps(key.interestOps() | op);
    }

    public static SelectionKey dstOf(SelectionKey key) {
        return (SelectionKey) key.attachment();
    }
}
