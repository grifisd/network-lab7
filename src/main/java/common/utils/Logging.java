package common.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.charset.StandardCharsets;

public class Logging {
    private static final Logger log = LogManager.getLogger(Logging.class);
    public static void logBuffer(ByteBuffer buffer) {
        log.trace("+++"
                + ": Buffer position/limit/capacity: "
                + buffer.position() + "/" + buffer.limit() + "/" + buffer.capacity());
        ByteBuffer dup = buffer.duplicate();
        dup.position(0);
        log.trace("+++"
                + ": Content: "
                + StandardCharsets.UTF_8.decode(dup).toString());
    }

}
