package common;

public class SharedConstants {
    public static final int bufferSize = 1<<10;
    public static final int maxSOCKSParamLength = 512;

    public static final byte supportedSOCKSVersion = 0x05;
    public static final byte[] supportedSOCKSAuthMethods = { 0x00 };
    public static final byte[] supportedSOCKSMethods = { 0x01 };
    public static final byte badSOCKSAuthMethodStub = (byte) 0xff;

    public class AddressType {
        public static final byte IPv4 = 0x01;
        public static final byte DNS = 0x03;
        public static final byte IPv6 = 0x04;
    }

    public class SOCKSStatus {
        public static final byte requestGranted = 0x00;
        public static final byte generalFailure = 0x01;
        public static final byte connectionNotAllowed = 0x02;
        public static final byte networkUnreacheable = 0x03;
        public static final byte hostUnreacheable = 0x04;
        public static final byte connectionRefused = 0x05;
        public static final byte ttlExpired = 0x06;
        public static final byte commandNotSupported = 0x07;
        public static final byte addressNotSupported = 0x08;
    }

    public static final byte[] localhostIPv4 = { 0x01, 0x00, 0x00, 0x7F };
    public static final byte[] localhostDomainName = "localhost".getBytes();
    public static final byte[] localhostIPv6 = { 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

    public static final byte[] zeroIPv4 = {0x00, 0x00, 0x00, 0x00};
    public static final byte[] zeroPort = {0x00, 0x00};
}