package storage;

import java.nio.channels.SelectionKey;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import static common.utils.SelectionKeys.dstOf;

@SuppressWarnings("SuspiciousMethodCalls")
public class KeyValueStorage<ValueType extends Removable> {

    private final Map<SelectionKey, ValueType> connections;

    public KeyValueStorage() {
        connections = new ConcurrentHashMap<>();
    }

    public Optional<ValueType> getAnyByKey(SelectionKey key) {
        ValueType connection = connections.get(key);
        if (null == connection) {
            connection = connections.get(key.attachment());
            if (null == connection)
                return Optional.empty();
        }
        return Optional.of(connection);
    }

    public void add(SelectionKey key, ValueType data) {
        RemoverDelegate removerDelegate = new DefaultRemover<>(this, key, data.hashCode());
        data.setRemoverDelegate(removerDelegate);
        connections.put(key, data);
    }

    void removeByKeyHash(SelectionKey key, int valueHash) {
        connections.entrySet().stream()
                .filter(selectionKeyValueTypeEntry ->
                        (selectionKeyValueTypeEntry.getKey().equals(key)
                        || selectionKeyValueTypeEntry.getKey().equals(dstOf(key)))
                        && selectionKeyValueTypeEntry.getValue().hashCode() == valueHash)
                .forEach(selectionKeyValueTypeEntry ->
                        connections.remove(selectionKeyValueTypeEntry.getKey(), selectionKeyValueTypeEntry.getValue()));
    }

    public int count() {
        return connections.size();
    }

}