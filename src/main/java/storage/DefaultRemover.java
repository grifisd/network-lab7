package storage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.ref.WeakReference;
import java.nio.channels.SelectionKey;

public class DefaultRemover<ValueType extends Removable> implements RemoverDelegate {
    private final Logger log = LogManager.getLogger(this.toString());
    private final SelectionKey removableKey;
    private final int valueHash;
    private final WeakReference<KeyValueStorage<ValueType>> weakStorage;

    DefaultRemover(KeyValueStorage<ValueType> storage, SelectionKey removableKey, int valueHash) {
        this.weakStorage = new WeakReference<>(storage);
        this.removableKey = removableKey;
        this.valueHash = valueHash;
    }

    @Override
    public void removeMe() {
        log.trace("Removing data with key " + removableKey);
        KeyValueStorage<ValueType> strongStorage = weakStorage.get();
        if (null == strongStorage) {
            log.fatal("Cannot find KeyValueStorage");
            return;
        }
        strongStorage.removeByKeyHash(removableKey, valueHash);
    }
}
