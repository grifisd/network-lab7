package storage;

public interface Removable {
    void setRemoverDelegate(RemoverDelegate removerDelegate);
}
