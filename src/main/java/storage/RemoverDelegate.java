package storage;

public interface RemoverDelegate {
    void removeMe();
}
