#!/bin/bash
tmp_dir=`mktemp -d`
tmp_filename="$tmp_dir"/fifo
mkfifo "$tmp_filename"
printf "tmp_filename=%s\n" "$tmp_filename"  # just to know the path to the fifo, it may be useful later

nc $1 $2 < "$tmp_filename" &
ncpid=$!  # PID may be useful later
printf "ncpid=%s\n" "$ncpid"

exec 3> "$tmp_filename"
